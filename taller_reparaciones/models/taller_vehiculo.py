# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _
from dateutil.relativedelta import relativedelta
import re

logger = logging.getLogger(__name__)

class TallerVehiculo(models.Model):
    _name = 'taller.vehiculo'
    _description = 'Vehiculo'

    matricula = fields.Char('Matrícula', required=True)
    marca = fields.Many2one('taller.marca', string="Marca")
    modelo = fields.Char('Modelo')
    fecha_recogido = fields.Date('Fecha de llegada', default=fields.Date.today)
    reparaciones_id = fields.One2many('taller.reparacion', 'vehiculo_id',string="Reparaciones del vehículo")
    state = fields.Selection([
        ('pedido', 'Pedido'),
        ('observacion', 'Observacion'),
        ('reparando', 'Reparando'),
        ('reparado', 'Reparado'),
        ('entregado', 'Entregado')
    ], 'State', default="pedido", group_expand='_expand_states')
    total_importe = fields.Float(string="Importe Total", compute='_compute_total', store=False, compute_sudo=False)
    cliente_id = fields.Many2one('taller.cliente', string="Propietario")
    fecha_estimada = fields.Date(string='Fecha estimada', compute='_compute_date', store=False, compute_sudo=False)

    def _expand_states(self, states, domain, order):
        return [key for key, val in type(self).state.selection]

    #@api.onchange('state')
    #def on_change_state(self):
    #    change_state(self.state)

    @api.multi
    def write(self, vals):
        if 'state' in vals:
            new_state = self.env['taller.vehiculo'].browse(vals['state'])
            for r in self:
                res = r.is_allowed_transiction(self.state, str(new_state)[17:-3])
                if not res:
                    message = _('No esta permitido el cambio de %s a %s') % (self.state, str(new_state)[17:-3])
                    raise UserError(message)
        result = super(TallerVehiculo, self).write(vals)
        return result

    @api.constrains('matricula')
    def _check_matricula(self):
        for record in self:
            expresion = '^([A-Z]{1,2})?\d{4}([A-Z]{2,3})$'
            if not re.search(expresion, record.matricula):
                raise models.ValidationError('Formato de matrícula no valido')

    @api.depends('reparaciones_id.importe')
    def _compute_total(self):
        for record in self:
            total = 0
            for line in record.reparaciones_id:
                total += line.importe
            
            record.total_importe = total

    @api.depends('reparaciones_id.dias_estimados')
    def _compute_date(self):
        for record in self:
            initial_date = record.fecha_recogido
            for line in record.reparaciones_id:
                initial_date = initial_date + relativedelta(days=line.dias_estimados)
            
            record.fecha_estimada = initial_date

    @api.model
    def is_allowed_transiction(self, old_state, new_state):
        allowed = [('pedido', 'observacion'),
                    ('observacion', 'reparando'),
                    ('reparando', 'reparado'),
                    ('reparado', 'entregado'),
                    ('reparando', 'observacion')]

        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        for vehiculo in self:
            if vehiculo.is_allowed_transiction(vehiculo.state, new_state):
                vehiculo.state = new_state
            else:
                message = _('No esta permitido el cambio de %s a %s') % (vehiculo.state, new_state)
                raise UserError(message)

    def make_observacion(self):
        self.change_state('observacion')

    def make_reparando(self):
        self.change_state('reparando')

    def make_reparado(self):
        self.change_state('reparado')

    def make_entregado(self):
        self.change_state('entregado')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(TallerVehiculo, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        self.registrar_marcas()
        return res

    def registrar_marcas(self):
        if self.env['taller.marca'].search([('nombre', '=', 'Dacia')]).nombre == False:
            dacia = {
                'nombre' : 'Dacia'
            }
            bmv = {
                'nombre' : 'BMV'
            }
            isuzu = {
                'nombre' : 'Isuzu'
            }
            opel = {
                'nombre' : 'Opel'
            }
            jaguar = {
                'nombre' : 'Jaguar'
            }
            self.env['taller.marca'].create(dacia)
            self.env['taller.marca'].create(bmv)
            self.env['taller.marca'].create(isuzu)
            self.env['taller.marca'].create(opel)
            self.env['taller.marca'].create(jaguar)

