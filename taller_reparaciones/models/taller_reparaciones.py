# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class TallerReparacion(models.Model):
    _name = 'taller.reparacion'
    _description = 'reparaciones'

    resumen = fields.Char('Resumen')
    descripcion = fields.Text('Descripcion')
    importe = fields.Float('Importe')
    dias_estimados = fields.Integer('Tiempo estimado(dias)')
    vehiculo_id = fields.Many2one('taller.vehiculo', ondelete='cascade')

    @api.constrains('dias_estimados')
    def solo_positivo(self):
        for record in self:
            if record.dias_estimados < 0:
                raise models.ValidationError('Los dias estimados no pueden ser inferiores a 0')