# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

class TallerMarca(models.Model):
    _name = 'taller.marca'
    _description = 'marcas'
    _rec_name = 'nombre'

    nombre = fields.Char('Marca', required=True)

    