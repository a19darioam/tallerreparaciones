# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _

class TallerCliente(models.Model):
    _name = 'taller.cliente'
    _description = 'Cliente'
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner', ondelete='cascade')
    vehiculo_ids = fields.One2many('taller.vehiculo', 'cliente_id', ondelete="restrict", string="Vehículos")
    conocio = fields.Char('Como nos conocio')