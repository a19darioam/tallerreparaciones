# -*- coding: utf-8 -*-
{
    'name': "Taller de Vehículos",

    'summary': """
        Modulo que maneja los vehiculos y sus reparaciones 
        de un taller automovilistico.""",

    'description': """
        Tener presente los vehiculos y los estados por los que pasa su reparacion.
        Llevar la cuenta de las reparaciones asociadas a los vehiculos.
        Guardar un pequeño registro de los propietarios de los mencionado vehículos.
    """,

    'author': "Dario Aldrey Muiño",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/taller_vehiculo.xml',
        'views/taller_reparaciones.xml',
        'views/taller_clientes.xml',
        'views/taller_marca.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}