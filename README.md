# TallerReparaciones

## Tematica
Modulo que maneja los vehículos y sus reparaciones en un taller automovilístico.

## Modelos/Vistas
El modulo consta de 4 modelos/vistas: vehiculos, reparaciones, cliente, marca.

- Vehiculos almacena datos sobre el vehiculo. Campos a resaltar: total_importe(Campo calculado a partir de los importes en reparaciones), matricula(Posee una constreint, para que se adapte a un patrón). Se han diseñado cuatro vistas: kanbas, calendar, tree e informe. Poseé un search que permite filtrar por matrícula. Se ha sobreescrito el método write para que en el kanbas el cambio de estados responda de la misma manera que en el formulario.

- Reparaciones almacena un resumen, descripción e importe de las reparaciones. El campo importe de este modelo es el usado para calcular el total_reparación de Vehiculo.

- Cliente, almacena los propietarios de los vehiculos. Heredan de res.partner(Modelo con la información habitual para clientes). Ligado a distintos vehículos.

- Marca. La primera vez que se inicializa el modulo se guardan una serie de marcas por defecto.

## Elementos de Código

- _check_matricula: Comprueba la validez de la matrícula
- _compute_total: Suma los importes de las reparaciones para calcular el total_importe del vehículo. Se ejecuta cada vez que un campo reparaciones_id.importe es modificado.
- _compute_date: Calcula la fecha estimada a partir del tiempo estimado de las diferentes reparaciones.
- _is_allowed_transictino: Comprueba que un cambio de estado sea valido.
- change_state: Permite el cambio de estado, es usado por los botones del formulario.
- write: ha sido sobreescrito con el objetivo de que cuando se haga un cambio de estado desde el kambas antes se compruebe si es valido.
- _expand_states junto con el "group_expand='_expand_states'" en los estados para que todos los estados aparezcan en el kambas aun cuando no hay ningún elemento con ese estado
- fields_view_get: usado para registrar las marcas.

## Añadido

- Campo 'dias_estimados' en el modelo taller.reparaciones. No puede tomar valores negativos.
- Campo 'fecha_estimada' en el modelo taller.vehiculos, se calcula a partir del conjunto de valores en los campos 'dias_estimados' del modelo taller.reparaciones asociadas al vehículo.
- Vista calendar que permite visualizar los vehiculos en la fecha_estimada